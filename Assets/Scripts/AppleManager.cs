﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class AppleManager : MonoBehaviour
{
    public Text levelCleard;
    public GameObject transition;
    public Text TotalManzanas;
    public Text FaltanManzanas;
    private int TotalManzanasInLevel;

    private void Start()
    {
        TotalManzanasInLevel = transform.childCount;
    }

    private void Update()
    {
        AllAppleCollected();
        TotalManzanas.text = TotalManzanasInLevel.ToString();
        FaltanManzanas.text = transform.childCount.ToString();
    }
    public void AllAppleCollected()
    {
        if (transform.childCount==0)
        {
            levelCleard.gameObject.SetActive(true);
            transition.SetActive(true);
            Invoke("ChangeScene", 1);
            
        }
    }

    void ChangeScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }


}
