﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerRespawn : MonoBehaviour
{

    public GameObject[] hearts;
    private int life;
    private float checkPositionX, checkPointPositicionY;
    public Animator animator;
    public Text gameOver;
    public Text reset;


    void Start()
    {
        life = hearts.Length;
        if (PlayerPrefs.GetFloat("checkPositionX")!=0)
        {
            transform.position = (new Vector2(PlayerPrefs.GetFloat("checkPositionX"), PlayerPrefs.GetFloat("checkPositionY")));
        }
    }

    private void CheckLife()
    {
        if (life<1)
        {
            Destroy(hearts[0].gameObject);
            animator.Play("Hit");
            //SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            transform.position = (new Vector2(-4, 1));
            gameOver.gameObject.SetActive(true);
            reset.gameObject.SetActive(true);
        }
        else if (life<2)
        {
            Destroy(hearts[1].gameObject);
            animator.Play("Hit");
        }
        else if (life < 3)
        {
            Destroy(hearts[2].gameObject);
            animator.Play("Hit");
        }
    }

    public void ReachedCheckPoint(float x, float y)
    {
        PlayerPrefs.SetFloat("checkPositionX", x);
        PlayerPrefs.SetFloat("checkPositionY", y);
    }

    public void PlayerDamage()
    {
        life--;
        CheckLife();
        
    }
}
