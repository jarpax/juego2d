﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FruitCollecter : MonoBehaviour
{

    public AudioSource clip;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            GetComponent<SpriteRenderer>().enabled = false;
            gameObject.transform.GetChild(0).gameObject.SetActive(true);
            FindObjectOfType<AppleManager>().AllAppleCollected();

            Destroy(gameObject, 0.5f);

            clip.Play();
        }
    }




}
