﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerMove : MonoBehaviour
{
    public float runSpeed = 2;
    public float jumpSpeed = 3;
    Rigidbody2D rb2d;
    public bool BetterJump = false;
    public float fallMiltiplie = 0.5f;
    public float lowJumpMultiplie = 1f;
    public SpriteRenderer spriteRenderer;
    public Animator animator;



    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
    }

    
    void FixedUpdate()
    {
        if (Input.GetKey("right"))
        {
            rb2d.velocity = new Vector2(runSpeed, rb2d.velocity.y);
            spriteRenderer.flipX = false;
            animator.SetBool("Run", true);
        }
        else if (Input.GetKey("left"))
        {
            rb2d.velocity = new Vector2(-runSpeed, rb2d.velocity.y);
            spriteRenderer.flipX = true;
            animator.SetBool("Run", true);
        }
        else
        {
            rb2d.velocity = new Vector2(0, rb2d.velocity.y);
            animator.SetBool("Run", false);
        }
        if (Input.GetKey(KeyCode.LeftShift) && Check.isGrounded)
        {
            rb2d.velocity = new Vector2(rb2d.velocity.x, jumpSpeed);
        }
        if (Check.isGrounded==false)
        {
            animator.SetBool("Jump", true);
            animator.SetBool("Run",false);
        }
        if (Check.isGrounded==true)
        {
            animator.SetBool("Jump",false);
        }
        if (Input.GetKey("r"))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }


        if (BetterJump)
        {
            if (rb2d.velocity.y<0)
            {
                rb2d.velocity += Vector2.up * Physics2D.gravity.y * (fallMiltiplie) * Time.deltaTime;
            }
            if (rb2d.velocity.y>0 && !Input.GetKey(KeyCode.LeftShift))
            {
                rb2d.velocity += Vector2.up * Physics2D.gravity.y * (lowJumpMultiplie) * Time.deltaTime;
            }
        }
    }
}
