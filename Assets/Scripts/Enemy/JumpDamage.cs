﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpDamage : MonoBehaviour
{

    public Collider2D collider2D;
    public Animator animator;
    public SpriteRenderer spiteRenderer;
    public GameObject destroyParticle;
    public float jumpForce = 2.5f;
    public int life = 2;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.CompareTag("Player"))
        {
            collision.gameObject.GetComponent<Rigidbody2D>().velocity = (Vector2.up * jumpForce);
            LosseLifeAnHit();
            CheckLife();

        }
    }

    public void LosseLifeAnHit()
    {
        life--;
        animator.Play("Hit");
    }

    public void CheckLife()
    {
        if (life==0)
        {
            destroyParticle.SetActive(true);
            spiteRenderer.enabled = false;
            Invoke("EnemyDie",0.2f);
        }
    }

    public void EnemyDie()
    {
        Destroy(gameObject);
    }

}
